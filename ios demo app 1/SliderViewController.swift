//
//  SliderViewController.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/06.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit

class SliderViewController: UIViewController {
    
    let setSliderColor = UIView()
       let redColorSlider = UISlider()
       let greenColorSlider = UISlider()
       let blueColorSlider = UISlider()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // カラーサンプルViewの作成
               setSliderColor.frame = CGRect(x: 10, y: 90, width: UIScreen.main.bounds.size.width-20, height: 100)
               setSliderColor.backgroundColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.0)
               self.view.addSubview(setSliderColor)
               setSliderColor.layer.borderWidth = 1.0
               setSliderColor.layer.borderColor = UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor
               
               // 赤のスライダー作成
               redColorSlider.frame = CGRect(x: 32, y: 210, width: UIScreen.main.bounds.size.width-52, height: 10)
               redColorSlider.minimumValue = 0.0  // 最小値を0.0に設定
               redColorSlider.maximumValue = 1.0  // 最大値を1.0に設定
               redColorSlider.value = 0.0  // 初期値を0.0に設定
               // スライダーが変更されたときに呼び出されるまメソッドの設定
               redColorSlider.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
               self.view.addSubview(redColorSlider)
               redColorSlider.tag = 1  // タグを1に設定
               
               // 緑のスライダー作成
               greenColorSlider.frame = CGRect(x: 32, y: 260, width: UIScreen.main.bounds.size.width-52, height: 10)
               greenColorSlider.minimumValue = 0.0  // 最小値を0.0に設定
               greenColorSlider.maximumValue = 1.0  // 最大値を1.0に設定
               greenColorSlider.value = 0.0  // 初期値を0.0に設定
               // スライダーが変更されたときに呼び出されるまメソッドの設定
               greenColorSlider.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
               self.view.addSubview(greenColorSlider)
               greenColorSlider.tag = 2  // タグを2に設定
               
               // 青のスライダー作成
               blueColorSlider.frame = CGRect(x: 32, y: 310, width: UIScreen.main.bounds.size.width-52, height: 10)
               blueColorSlider.minimumValue = 0.0  // 最小値を0.0に設定
               blueColorSlider.maximumValue = 1.0  // 最大値を1.0に設定
               blueColorSlider.value = 0.0  // 初期値を0.0に設定
               // スライダーが変更されたときに呼び出されるまメソッドの設定
               blueColorSlider.addTarget(self, action: #selector(changeColorSlider), for: UIControl.Event.valueChanged)
               self.view.addSubview(blueColorSlider)
               blueColorSlider.tag = 3  // タグを3に設定
               
               // 赤の見出しViewの作成
               let redSliderColor = UIView()
               redSliderColor.frame = CGRect(x: 12, y: 218, width: 12, height: 12)
               redSliderColor.backgroundColor = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.0)
               self.view.addSubview(redSliderColor)
               redSliderColor.layer.borderWidth = 1.0
               redSliderColor.layer.borderColor = UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor
               
               // 緑の見出しViewの作成
               let greenSliderColor = UIView()
               greenSliderColor.frame = CGRect(x: 12, y: 258, width: 12, height: 12)
               greenSliderColor.backgroundColor = UIColor(red: 0.00, green: 1.00, blue: 0.00, alpha: 1.0)
               self.view.addSubview(greenSliderColor)
               greenSliderColor.layer.borderWidth = 1.0
               greenSliderColor.layer.borderColor = UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor
               
               // 青の見出しViewの作成
               let blueSliderColor = UIView()
               blueSliderColor.frame = CGRect(x: 12, y: 298, width: 12, height: 12)
               blueSliderColor.backgroundColor = UIColor(red: 0.00, green: 0.00, blue: 1.00, alpha: 1.0)
               self.view.addSubview(blueSliderColor)
               blueSliderColor.layer.borderWidth = 1.0
               blueSliderColor.layer.borderColor = UIColor(red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor

        
        
        //self.view.backgroundColor = UIColor.white
        
        

        // Do any additional setup after loading the view.
    }
    
    // カラースライダー変更処理
       @objc func changeColorSlider(_ sender: UISlider) {
           // タグで識別
           switch (sender.tag) {
           case 1:
               print("赤のスライダーの値: \(sender.value)")
           case 2:
               print("緑のスライダーの値: \(sender.value)")
           case 3:
               print("青のスライダーの値: \(sender.value)")
           default:
               break
           }
           // カラーサンプルの色変更
           setSliderColor.backgroundColor = UIColor(red: CGFloat(redColorSlider.value), green: CGFloat(greenColorSlider.value), blue: CGFloat(blueColorSlider.value), alpha: 1.0)
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
