//
//  FormViewController.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/07.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
//UIPickerViewDataSource
class FormViewController: UIViewController, UIPickerViewDelegate {
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var textField2: UITextField!
    
    @IBOutlet weak var textFieldname: UITextField!
    
    
    @IBOutlet weak var dateField: UITextField!
    
    var datePicker: UIDatePicker = UIDatePicker()
    
    @IBOutlet weak var nenrei: UITextField!
    
    @IBOutlet weak var textnet: UILabel!
    
    @IBOutlet weak var textman: UILabel!
    
    @IBOutlet weak var textwoman: UILabel!
    
    @IBOutlet weak var textother: UILabel!
    
    @IBOutlet weak var textnone: UILabel!
    
    @IBOutlet weak var checknet: CheckBox!
    
    @IBOutlet weak var internet: UILabel!
    
    @IBOutlet weak var checkkizi: CheckBox1!
    
    @IBOutlet weak var textkizi: UILabel!
    
    @IBOutlet weak var checkfriend: CheckBox2!
    
    
    @IBOutlet weak var textfriend: UILabel!
    
    @IBOutlet weak var checksemina: CheckBox3!
    
    @IBOutlet weak var textsemina: UILabel!
    
    @IBOutlet weak var checkother: CheckBox4!
    
    @IBOutlet weak var textotherc: UILabel!
    
    
    @IBOutlet weak var nnenrei: UILabel!
    //    var pickerView: UIPickerView = UIPickerView()
//    var pickerView1: UIPickerView = UIPickerView()
//    var pickerView2: UIPickerView = UIPickerView()
//
//    let list = ["", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009"]
//
//    let listtuki = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
//
//    let listniti = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    
    var NumberOfButtons: Int = 3 //ボタンの数
    var CheckedButtonTag = 3 //チェックされているボタンのタグ
    //それぞれ画像を読み込む
    let checkedImage = UIImage(named: "check_on")! as UIImage
    let uncheckedImage = UIImage(named: "check_off")! as UIImage

    //ラジオボタンを配置する
    func set_radiobutton(num:Int){
        let button = UIButton()
        let center = Int(UIScreen.main.bounds.size.width / 2)  //中央の位置
        let y = 170+45*num  //ボタン同士が重ならないようyを調整
        button.setImage(uncheckedImage, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(butttonClicked(_:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: center - 120,
                              y: y,
                              width: 60,
                              height: 40)
        button.tag = num
        self.view.addSubview(button)
    }

    //押されたボタンの画像をcheck_on.pngに変える
    @objc func butttonClicked(_ sender: UIButton) {
        ChangeToUnchecked(num: CheckedButtonTag)
        let button = sender
        button.setImage(checkedImage, for: UIControl.State.normal)
        CheckedButtonTag = button.tag  //check_on.pngになっているボタンのtagを更新
    }

    //すでにcheck_on.pngになっているボタンの画像をcheck_off.pngに変える
    func ChangeToUnchecked(num:Int){
        for v in view.subviews {
            if let v = v as? UIButton, v.tag == num {
                v.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0..<NumberOfButtons {
            set_radiobutton(num: i)
        }
        
//        pickerView.delegate = self
//        pickerView.dataSource = self
//        pickerView.showsSelectionIndicator = true
//        pickerView.tag = 0
//
//            let toolbar = UIToolbar(frame: CGRectMake(0, 0, 0, 35))
//            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(FormViewController.done))
//            let cancelItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FormViewController.cancel))
//            toolbar.setItems([cancelItem, doneItem], animated: true)
//
//            self.textField.inputView = pickerView
//            self.textField.inputAccessoryView = toolbar
//
//        //piclerView1の設定
//        pickerView1.delegate = self
//        pickerView1.dataSource = self
//        pickerView1.showsSelectionIndicator = true
//        pickerView1.tag = 1
//
//        let toolbar1 = UIToolbar(frame: CGRectMake(0, 0, 0, 35))
//        let doneItem1 = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(FormViewController.done))
//        let cancelItem1 = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FormViewController.cancel))
//        toolbar1.setItems([cancelItem1, doneItem1], animated: true)
//
//        self.textField1.inputView = pickerView1
//        self.textField1.inputAccessoryView = toolbar1
//
//        pickerView2.delegate = self
//        pickerView2.dataSource = self
//        pickerView2.showsSelectionIndicator = true
//        pickerView2.tag = 2
//
//        let toolbar2 = UIToolbar(frame: CGRectMake(0, 0, 0, 35))
//        let doneItem2 = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(FormViewController.done))
//        let cancelItem2 = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FormViewController.cancel))
//        toolbar2.setItems([cancelItem2, doneItem2], animated: true)
//
//        self.textField2.inputView = pickerView2
//        self.textField2.inputAccessoryView = toolbar2
        
        
        
        
        // ピッカー設定
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = Locale.current
        dateField.inputView = datePicker

        // 決定バーの生成
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let spacelItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        toolbar.setItems([spacelItem, doneItem], animated: true)

        // インプットビュー設定(紐づいているUITextfieldへ代入)
        dateField.inputView = datePicker
        dateField.inputAccessoryView = toolbar
        //ViewDidload終わり↓
        }
    // UIDatePickerのDoneを押したら発火
    @objc func done() {
        dateField.endEditing(true)

        // 日付のフォーマット
        let formatter = DateFormatter()

        //"yyyy年MM月dd日"を"yyyy/MM/dd"したりして出力の仕方を好きに変更できるよ
        formatter.dateFormat = "yyyy年MM月dd日"

        //(from: datePicker.date))を指定してあげることで
        //datePickerで指定した日付が表示される
        dateField.text = "\(formatter.string(from: datePicker.date))"


        // 本当はNSDate()じゃない方がいいと思う。
        let now = Date()
        print("今日は\(now))です")

        let Age = now.timeIntervalSince(datePicker.date)//生まれてからの秒数

        let myAge2 = Int(Age)//秒齢
        let myAge3 = Double(myAge2)
        let myAge4 = Int(myAge2/60/60/24)//日齢
        let myAge5 = Int(myAge3/60/60/24/365.24)//年齢＿端数の切り捨て:満年齢:整数Integer
        nenrei.text = String(myAge5)
        nnenrei.text = String(myAge5)

        



    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ①セグエ実行前処理
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
           // ②Segueの識別子確認
           if segue.identifier == "nextform2" {
    
               // ③遷移先ViewCntrollerの取得
               let nextView = segue.destination as! Form2ViewController
    
               // ④値の設定
               nextView.argString = textFieldname.text!
            
               // nextView.Secondtext = textField.text!
            
          //  nextView.month = textField1.text!
            
            nextView.nenrei = nenrei.text!
            nextView.seinengappitext = dateField.text!
            
           // nextView.day = textField2.text!
            
            if CheckedButtonTag == 0 {
                nextView.seibetu = textman.text!
                
            }else if CheckedButtonTag == 1{
                nextView.seibetu = textwoman.text!
                
            }else if CheckedButtonTag == 2{
                nextView.seibetu = textother.text!
                
            }else {
                nextView.seibetu = textnone.text!
            }
            
            if checknet.isChecked{
                nextView.net = internet.text!
                
            }else{
                nextView.net = textnone.text!
            }
            
            if checkkizi.isChecked{
                nextView.kizi = textkizi.text!
                
            }else{
                nextView.kizi = textnone.text!
            }
            
            if checkfriend.isChecked{
                nextView.friend = textfriend.text!
                
            }else{
                nextView.friend = textnone.text!
            }
            
            if checksemina.isChecked{
                nextView.semina = textsemina.text!
                
            }else{
                nextView.semina = textnone.text!
            }
            
            if checkother.isChecked{
                nextView.other = textotherc.text!
                
            }else{
                nextView.other = textnone.text!
            }
           }
       }
    
    //幾つの項目をピッカーに表示するか指定する

//        func numberOfComponents(in pickerView: UIPickerView) -> Int {
//            return 1
//        }
//
//        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//            if pickerView.tag == 0 {
//                return list.count
//
//            }else if pickerView.tag == 1{
//            return listtuki.count
//
//            }else {
//            return listniti.count
//
//        }
//    }
//
//
//        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//            if pickerView.tag == 0 {
//                    return list[row]
//
//                }else if pickerView.tag == 1{
//                return listtuki[row]
//
//                }else {
//                return listniti[row]
//
//            }
//
//        }
//
//        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//
//            if pickerView.tag == 0 {
//                    self.textField.text = list[row]
//
//                }else if pickerView.tag == 1{
//                self.textField1.text = listtuki[row]
//
//                }else {
//                self.textField2.text = listniti[row]
//
//            }
//
//        }
//
//
//
//    @objc func cancel() {
//            self.textField.text = ""
//            self.textField.endEditing(true)
//
//            self.textField1.text = ""
//            self.textField1.endEditing(true)
//
//            self.textField2.text = ""
//            self.textField2.endEditing(true)
//        }
//
//    @objc func done() {
//            self.textField.endEditing(true)
//
//            self.textField1.endEditing(true)
//
//            self.textField2.endEditing(true)
//        }
//
//        func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
//            return CGRect(x: x, y: y, width: width, height: height)
//        }
//
//        override func didReceiveMemoryWarning() {
//            super.didReceiveMemoryWarning()
//            // Dispose of any resources that can be recreated.
//        }
//
    

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


