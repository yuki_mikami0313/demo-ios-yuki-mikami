//
//  CheckBox.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/08.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit



class CheckBox: UIButton {
    // Images
    let checkedImagec = UIImage(named: "check_on")! as UIImage
    let uncheckedImagec = UIImage(named: "check_off")! as UIImage

    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImagec, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImagec, for: UIControl.State.normal)
            }
        }
    }

    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
    }

    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
