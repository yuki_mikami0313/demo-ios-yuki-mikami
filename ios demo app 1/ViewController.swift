//
//  ViewController.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/03.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit

//追加①
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //追加②
    let TODO = ["Input Text", "Slider", "Drag and Drop","Map View","Play Video","Web View","View Pager","Grid View","Unquete","Scroll"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    //追加③ セルの個数を指定するデリゲートメソッド（必須）
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TODO.count
    }
    
    //追加④ セルに値を設定するデータソースメソッド（必須）
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // セルを取得する
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "table_cell", for: indexPath)
        // セルに表示する値を設定する
        cell.textLabel!.text = TODO[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
           // タップされたセルの行番号を出力
        //print("\(indexPath.row)番目の行が選択されました。")
           
        if(indexPath.row == 1){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextslider", sender: self)
        }else if(indexPath.row == 3){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextgmap", sender: self)
        }else if(indexPath.row == 4){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextvideo", sender: self)
        }else if(indexPath.row == 5){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextweb", sender: self)
        }else if(indexPath.row == 6){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextscroll", sender: self)
        }else if(indexPath.row == 0){
            // セルの選択を解除
            
            tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
            performSegue(withIdentifier: "nextform", sender: self)
        }
        // 次の画面のBackボタンを「戻る」に変更
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
            title:  "戻る",
            style:  .plain,
            target: nil,
            action: nil
        )
                  
       }
    
    
    
    
    
    


}

