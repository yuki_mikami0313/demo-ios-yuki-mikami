//
//  MapViewController.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/06.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
// MapKit framework をimport
import MapKit


class MapViewController: UIViewController, MKMapViewDelegate {

   @IBOutlet var mapView:MKMapView!
    override func loadView() {
        
        // 緯度・軽度を設定
                  let location:CLLocationCoordinate2D
                      = CLLocationCoordinate2DMake(35.68154,139.752498)
           
                  mapView.setCenter(location,animated:true)
           
                  // 縮尺を設定
                  var region:MKCoordinateRegion = mapView.region
                  region.center = location
                  region.span.latitudeDelta = 0.02
                  region.span.longitudeDelta = 0.02
           
                  mapView.setRegion(region,animated:true)
           
                  // 表示タイプを航空写真と地図のハイブリッドに設定
                  mapView.mapType = MKMapType.hybrid
                  // mapView.mapType = MKMapType.standard
                  // mapView.mapType = MKMapType.satellite
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
