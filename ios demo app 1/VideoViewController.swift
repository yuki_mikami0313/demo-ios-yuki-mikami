//
//  VideoViewController.swift
//  ios demo app 1
//
//  Created by ymikami on 2020/04/06.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
 
class VideoViewController: UIViewController {
 
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let button = UIButton(type: .system)
        button.setTitle("Play", for: .normal)
        button.addTarget(self, action: #selector(self.playMovieFromProjectBundle), for: .touchUpInside)
 
        // Documentディレクトリから動画を読み込む場合はこちら
        // button.addTarget(self, action: #selector(self.playMovieFromLocalFile), for: .touchUpInside)
 
        button.sizeToFit()
        button.center = self.view.center
        self.view.addSubview(button)
    }
     
    // アプリ内に組み込んだ動画ファイルを再生
    @objc func playMovieFromProjectBundle() {
         
        let asset = NSDataAsset(name:"clip")
        let videoUrl = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("clip.mp4")
        try! asset!.data.write(to: videoUrl)
        let item = AVPlayerItem(url: videoUrl)
        let playerController = AVPlayerViewController()
        playerController.player = AVPlayer(playerItem: item)
        self.present(playerController, animated: true, completion: {
            playerController.player?.play()
        })
    }
 
    // アプリのDocumentディレクトリにある動画ファイルを再生
    func playMovieFromLocalFile() {
         
        let path = "\(getDocumentDirectory())/file.mp4"
         
        let videoPlayer = AVPlayer(url: URL(fileURLWithPath: path))
         
        // 動画プレイヤーの用意
        let playerController = AVPlayerViewController()
        playerController.player = videoPlayer
        self.present(playerController, animated: true, completion: {
            videoPlayer.play()
        })
    }
     
    // Documentディレクトリのパスを取得
    func getDocumentDirectory() -> String {
     
        return NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
